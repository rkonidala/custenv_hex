#!/usr/bin/env python
from __future__ import print_function
import click
import subprocess
import os

script_dir = os.path.dirname(__file__)
playbooks_dir = os.path.join(script_dir, 'plays')
hosts_dir = os.getcwd()
hosts_path = os.path.join(hosts_dir, 'hosts')

@click.group()

def main():
    """
    An MVP to display the products, versions and count of servers in a customer environment
    Currently only payor, connector, answers and database products are supported
    """
    pass

@main.command()
@click.argument(
    'product',
    type = str,
    default = 'all',
    )
@click.option('--display-version', '-v', is_flag = True, help = "Flag to display version information")
@click.option('--display-count', '-c', is_flag = True)
def list(product, display_version = False, display_count = False):
    """
    Display the products, versions and count of servers based on product category
    """
    products_playbook = 'get_installed_products.yml'
    products_playbook_path = os.path.join(playbooks_dir, products_playbook)
    version_playbook = 'get_version.yml'
    version_playbook_path = os.path.join(playbooks_dir,version_playbook)
    report_playbook = 'print_report.yml'
    report_playbook_path = os.path.join(playbooks_dir, report_playbook)
    
    print("products installed are {}".format(product))
    run_playbook(products_playbook_path, hosts_path)
    if display_count:
        print("count: xx")
    if display_version:
        run_playbook(version_playbook_path, hosts_path)
    run_playbook(report_playbook_path, hosts_path)
        

# Method to run the ansible_playbook
def run_playbook(playbook, hosts='./hosts', vars=False,verbosity=False,playbook_tags=False):
    if type(vars) is dict:
        playbook_command = "ansible-playbook -i %s %s -e" % (hosts, playbook)
        playbook_command = playbook_command.split()
        playbook_command.append(str(vars))        
    else:
        playbook_command = "ansible-playbook -i %s %s" % (hosts, playbook)
        playbook_command = playbook_command.split()
    # Run the playbook with tags
    if playbook_tags:
        playbook_command.append("--tags")
        playbook_command.append(playbook_tags)
    # Run in verbose mode when defined
    if verbosity:
        playbook_command.append("-vvv")
    print("Running Playbook:\n{}".format(' '.join(playbook_command)))
    return subprocess.call(playbook_command)

def get_version():
    playbook

if __name__=="__main__":
    main()

