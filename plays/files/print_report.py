#!/usr/bin/env python
from __future__ import print_function
import os
try:
    from ruamel.yaml import YAML
except ImportError:
    raise ImportError('cannot import ruamel.yaml')
import sys

"""
Description: 
  - This script is to format the key, value pairs in yaml file to print summary in a tabular form for payor-post validation.
Usage: 
  format_report.py <report yaml file>
Example:
  format_report.py payor_pdv_consolidated_report.yml
Note:
  - Only accepts the report yaml file as an argument
"""

def main():
    report_file_path = sys.argv[1]
    dash = '-'*71
    if not os.path.exists(report_file_path):
        print("{} doesn't exist".format(report_file_path))
        sys.exit(1)
    yaml = YAML()
    with open(report_file_path, 'r') as f:
        yml_op = yaml.load(f)
    print(dash)
    print("{:<40}|{:^30}".format('Product Category','State'))
    print(dash)
    for key,value in yml_op.iteritems():
        print("{:<40}|{:^30}".format(key,value))

# Execution starts here
if __name__ == '__main__':
    main()

