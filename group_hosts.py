#!/usr/bin/env python
from __future__ import print_function
import os
import fnmatch
import sys
import shutil
import yaml
import argparse

"""
Description: 
  - This script is used to generate the product specific host files for both caremanager and payor.
  - If we dont pass any input files product files for all the environments will be generated.

Usage: 
  <path_to_script>/group_hosts.py -d <destination directory path> -f <path of yaml filter file>

Example:
  /tmp/group_hosts.py -d /tmp/testing -f filt_file.yml

Args:
  - (--dest_dir) or (-d) should be an absolute path of the destination directory where the host files should be generated.
    Required: True
  - (--input_file_path) or (-f) can be a relative or absolute path of the input yaml filter file. 
    Required: False (Defauls to all environments)

Note:
  - Script should always be run from environments repository
  - All the elements of the yaml file should be as a list of elements.
"""

def is_path(string):
    '''Check if path exists'''
    if not os.path.exists(string):
        print("ERROR: {} doesn't exist".format(string))
        sys.exit(1)

def is_repo_root(repo_name):
    '''Check if current path is repo root directory'''
    current_path=os.path.abspath('.')
    current_dir_name= os.path.basename(current_path)
    if repo_name != current_dir_name and not os.path.exists('.git'):
        print("ERROR: Not {} repository, exiting".format(repo_name))
        sys.exit(1)

def find_files(folder, pattern):
    '''
       Generate all the absolute paths of files matching the pattern
       Works similar to find command in bash
    '''
    for root, folders, files in os.walk(folder):
        for filename in folders + files:
            fpath = os.path.join(root, filename)
            fname = os.path.basename(fpath)
            if fnmatch.fnmatch(fname, pattern):
                yield os.path.abspath(fpath)

def parse_filt_file(file):
    '''Parses the input yaml filter file to generate a list of contents'''
    is_path(file)
    with open(file, 'r') as f:
        yml_data = yaml.load(f)
    return yml_data

def check_exact_match(path_string, sub_string):
    '''Checks if a sub_string contents are present in a string'''
    path_string_list = filter(lambda x: x != '', path_string.split('/'))
    path_string_set = set(path_string_list)
    sub_string_list = filter(lambda x: x != '', sub_string.split('/'))
    sub_string_set = set(sub_string_list)
    if sub_string_set.issubset(path_string_set):
        return True
    else:
        return False

def filter_hosts(host_files, env_patterns):
    '''Filters the path list with the alist of patterns'''
    filt_host_files = []
    for env in env_patterns:
        for file in host_files:
            if check_exact_match(file, env):
                filt_host_files.append(file)
    filt_host_files = list(set(filt_host_files))
    return filt_host_files

def generate_hosts(dest_dir_path, host_files):
    '''Generates the host files based on input path list and destination directory'''
    if not os.path.exists(dest_dir_path):
        os.mkdir(dest_dir_path)
    for path in host_files:
        rel_path_list = list(filter(lambda x: x != '', path.split('/')))
        rel_path_dir = os.path.join(dest_dir_path, rel_path_list[0], rel_path_list[1])
        print("{:<70} -->   {:<70}".format(path, rel_path_dir))
        if not os.path.exists(rel_path_dir):
            os.makedirs(rel_path_dir)
        src_file_path = path
        dest_file_path = os.path.join(rel_path_dir, 'hosts')

        if not os.path.exists(dest_file_path) and os.path.isdir(src_file_path):
            shutil.copytree(src_file_path, dest_file_path)
        if os.path.exists(dest_file_path) and os.path.isfile(src_file_path):
            dest_file_path = os.path.join(dest_file_path, 'cm_hosts')
            # os.mkdir(os.path.dirname(dest_file_path))
            shutil.copyfile(src_file_path, dest_file_path)

def main():
    parser = argparse.ArgumentParser(description='Generate installer host files from environments repo')
    parser.add_argument('-f', '--input_file_path', type=str, required=False, help='Input filter file path')
    parser.add_argument('-d', '--dest_path', type=str, required=True, help='Destination directory path for generated hosts')
    args = parser.parse_args()
    repo_name = "environments"
    hosts_search_pattern = "hosts"
    dest_dir = os.path.join(args.dest_path, 'gen_hosts')
    # cm_dest_dir = os.path.join(args.dest_path, 'hosts_cm')
    
    # Validate if destination directory exists
    is_path(args.dest_path)

    # Validate if running from environments repo
    is_repo_root(repo_name)
    
    # Find all host files in path
    host_file_list = list(find_files(os.path.curdir, hosts_search_pattern))
    host_relpath_list = [os.path.relpath(rel_path, os.path.curdir) for rel_path in host_file_list]

    # Filter product host files based on input filter file if exists
    if args.input_file_path is not None:
        env_pattern_list = parse_filt_file(args.input_file_path)
        host_relpath_list = filter_hosts(host_relpath_list, env_pattern_list)

    # Capture payor and caremanager host file list
    payor_host_list = filter(lambda path: os.path.isdir(path), host_relpath_list)
    cm_host_list = filter(lambda path: 'generated-cm-properties' in path, host_relpath_list)

    # Generate payor and caremanager host files
    generate_hosts(dest_dir, payor_host_list)
    generate_hosts(dest_dir, cm_host_list)

# execution starts here
if __name__ == '__main__':
    main()
